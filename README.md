# 3wire i2s to S/PDIF for Echo Dot, see:
https://hackaday.io/project/162309-spdif-from-echo-dot


There are two version of the PCB in here:
v03 - Tested - see https://hackaday.io/project/162309-spdif-from-echo-dot/log/156195-mounted-inside-echo-dot-done
Basically works (well enough for me), but has a few issues detailed in that link. In particular, the echo dot case doesn't quite close, and one track is too close to the edge of the board and got cut off.

v04 - *Untested* - but _should_ (maybe) fix the previously mentioned issues. The board is slightly smaller, and the problem track has been routed away from the edge of the board.





