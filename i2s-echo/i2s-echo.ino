/*
 * 
 * 3-wire i2s -> S/PDIF for Amazon Echo dot, see:
 *   https://hackaday.io/project/162309-spdif-from-echo-dot
 * 
 * Code is pretty simple:
 * 1. Enable the i2s input and S/PDIF Output on the WM8805
 * 2. Configure the cdce906 PLL to multiply the CLK_IN1 input by 4,
 *    which is BLCK from the Echo, and supplied from the PLL to the
 *    wm8805 as mclk.
 * 
 * Once everything is configured, it does nothing.
 * 
 * Tested with an atmega328p programmed with Arduino 1.8.5.
 * 
 * Parts of the WM8805 code based on:
 * https://github.com/ultranalog/wm8804_arduino
 * 
 * Daniel Swann, Nov 2018.
 * 
 */


#include <Wire.h>
// i2c addresses 
int cdce906 = 0x69;
int wm8804  = 0x3a;


void setup()
{
  Wire.begin();
  Serial.begin(9600);
  Serial.println("START!");
  delay(1000); 


  Serial.println("cdce906 init...");
  cdce906_init();
  delay(250);

  Serial.println("wm8804 init...");
  wm8804_init(wm8804);

  delay(2000);
}

void loop()
{
  
}

void cdce906_init()
{
  byte error;
  Wire.beginTransmission(cdce906);
  error = Wire.endTransmission();

  if (error == 0)
  {
    Serial.println("cdce906: found");
  }
  else if (error==4) 
  {
    Serial.println("cdce906: Unknown error");
  }
  else
  {
    Serial.println("cdce906: No response!");
  }

  byte c = cdce906_read_register(cdce906, 0);
  Serial.print("cdce906: ");
  Serial.print(c, HEX);
  Serial.print(" - Vendor ID: ");
  Serial.print(c & 0x0F);

  Serial.print(", Rev code: ");
  Serial.println((c & 0xF0) >> 4 );


  // Set "Input Signal Source"
  c = cdce906_read_register(cdce906, 11);
  c |= 1 << 6;
  c &= ~(1 << 7);
  cdce906_write_register(cdce906, 11, c);

  // Set Input Clock Selection (byte10, bit 4)
  c = cdce906_read_register(cdce906, 10);
  c |= 1 << 4;    // Clock source = CLK_IN1 (BCLK input)
//c &= ~(1 << 4); // Clock source = CLK_IN0 (MLCK input)
  cdce906_write_register(cdce906, 10, c);

  // Configure PLL - x4 input
  cdce906_write_register(cdce906, 1, 1);  // PLL1, M = 1
  cdce906_write_register(cdce906, 2, 32); // PLL1, N = 32
}

 
void wm8804_init(int devaddr)
{
  byte error;
  Wire.beginTransmission(wm8804);
  error = Wire.endTransmission();

  if (error == 0)
  {
    Serial.println("WM8804: found");
  }
  else if (error==4) 
  {
    Serial.println("WM8804: Unknown error at address 0x3A");
  }
  else
  {
    Serial.println("WM8804: No response!");
  }
  
  Serial.print("WM8804: Device ID: ");
  byte c = wm8804_read_register(wm8804, 1);
  if (c < 10) {
    Serial.print('0');
  } 
  Serial.print(c,HEX);
   
  c = wm8804_read_register(wm8804, 0);
  if (c < 10) {
    Serial.print('0');
  } 
  Serial.print(c,HEX);
  
  Serial.print(" Rev. ");
  c = wm8804_read_register(wm8804, 2);
  Serial.println(c,HEX);
    
  
  // resets, initializes and powers a wm8805
  // reset device
  wm8804_write_register(devaddr, 0, 0);

  // REGISTER 30
  // set the PWRDN
  // bit 7:6 - always 0
  // bit   5 - TRIOP      - Tri-state all Outputs => 0
  // bit   4 - AIFPD      - Digital Audio Interface Power Down => 0
  // bit   3 - OSCPD      - Oscillator Power Down => 0
  // bit   2 - SPDIFTXPD  - S/PDIF Transmitter Powerdown => 0
  // bit   1 - SPDIFRXPD  - S/PDIF Receiver Powerdown => 1 (off)
  // bit   0 - PLLPD      - PLL Powerdown => 0
  wm8804_write_register(devaddr, 30, B00001011);
}

byte cdce906_read_register(int devaddr, int regaddr)
{
  // Byte Read or Byte Write operation
  regaddr |= (1 << 7);
  
  Wire.beginTransmission(devaddr);
  Wire.write(regaddr);
  Wire.requestFrom(devaddr, 1); // only one byte
  byte data = Wire.read();
  Wire.endTransmission(true);
  return data;
}

void cdce906_write_register(int devaddr, int regaddr, int dataval)
{
  // Byte Read or Byte Write operation
  regaddr |= (1 << 7);
     
  // Write a data register value
  Wire.beginTransmission(devaddr); // device
  Wire.write(regaddr); // register
  Wire.write(dataval); // data
  Wire.endTransmission(true);
}

byte wm8804_read_register(int devaddr, int regaddr)
{
  // Read a data register value
  Wire.beginTransmission(devaddr);
  Wire.write(regaddr);
  Wire.endTransmission(false);                                            // repeated start condition: don't send stop condition, keeping connection alive.
  Wire.requestFrom(devaddr, 1); // only one byte
  byte data = Wire.read();
  Wire.endTransmission(true);
  return data;
}

void wm8804_write_register(int devaddr, int regaddr, int dataval)
{
  // Write a data register value
  Wire.beginTransmission(devaddr); // device
  Wire.write(regaddr); // register
  Wire.write(dataval); // data
  Wire.endTransmission(true);
}



