
| Designator           | Package                        | Quantity | Designation | Comments                                                |
| -------------------  | ------------------------------ | -------- | ----------- | ------------------------------------------------------- |
| C1,C2                | C_Rect_L7_W2.5_P5              |        2 | 100nF       |                                                         |
| C3,C4,C12,C13,C14,C9 | C_Rect_L7_W3.5_P5              |        6 | 100nF       |                                                         |
| C5,C6,C7,C11         | C_Disc_D3_P2.5                 |        4 | 15pF        |                                                         |
| C8,C10               | C_Disc_D6_P5                   |        2 | 1uF         |                                                         |
| CON1                 | Pin_Header_Straight_2x03       |        1 | AVR-ISP-6   |                                                         |
| D1                   | LED-5MM                        |        1 | LED         | (not used)                                              |
| IC1                  | TQFP-32_7x7mm_Pitch0.8mm       |        1 | ATMEGA328p  |                                                         |
| J1                   | FCR684204T                     |        1 | fcr684204t  | 3.5mm optical connector                                 |
| JP1                  | Socket_Strip_Straight_1x03     |        1 | MCLK sel    | select between generated MCLK and MCLK at i2s header    |
| P1                   | Socket_Strip_Straight_1x06     |        1 | FTDI        |                                                         |
| P2                   | Socket_Strip_Straight_1x03     |        1 | I²C         | (header not needed)                                     |
| P3                   | Socket_Strip_Straight_1x04     |        1 | I²S         |                                                         |
| P4                   | Socket_Strip_Straight_1x05     |        1 | GPIO        | (not used)                                              |
| P5                   | Socket_Strip_Straight_1x03     |        1 | Power       |                                                         |
| R2                   | R_0805_HandSoldering           |        1 | 68R         | (for LED / not needed if no LED)                        | 
| R3,R4                | R_0805_HandSoldering           |        2 | 4k7         |                                                         |
| R5,R6,R7,R8,R1       | R_0805_HandSoldering           |        5 | 10k         |                                                         |
| R9,R10,R11,R12       | R_0805_HandSoldering           |        4 | 100R        |                                                         |
| U1                   | SSOP-28_5.3x10.2mm_Pitch0.65mm |        1 | WM8805      | Digital audio transceiver                               |
| U2                   | SOT-23                         |        1 | LM3480-3.3  | 5v->3v3 reg.                                            |
| U3                   | TSSOP-20_4.4x6.5mm_Pitch0.65mm |        1 | CDCE906     | PLL                                                     |
| Y2                   | Crystal_HC49-U_Vertical        |        1 | 12M         | For WM8805                                              |
| Y1                   | Crystal_HC49-U_Vertical        |        1 | 16M         | For ATMEGA328p                                          |

